#!/usr/bin/env bash

cd /home/copr/src

export PKG_CONFIG_PATH="/home/copr/dev/share/pkgconfig:/home/copr/dev/lib/pkgconfig:$PKG_CONFIG_PATH"
export INSTALL_DIR=/home/copr/dev

echo "----------$(date)----------" >> /var/log/copr.log
for DIR in spice-protocol usbredir libcacard phodav spice spice-gtk spice-vdagent libgovirt virt-viewer xorg-x11-drv-qxl; do
  cd $DIR
  CONFIG_PARAMS=""
  if [[ $DIR = "spice-gtk" ]]; then
    CONFIG_PARAMS="--disable-polkit"
  fi
  git fetch
  git pull | grep -q -v 'Already up-to-date.'  &&  git clean -xfd  && ./autogen.sh --disable-werror --prefix="$INSTALL_DIR" "$CONFIG_PARAMS" && make -j 2 && make dist && ~/src/ci/build-in-copr.sh `ls *tar* | head -n1` @spice/nightly "$DIR" 
  if [[ "$?" = "0" ]]; then
    echo "$DIR was built" >> /var/log/copr.log
    case $DIR in
        "spice-protocol"|"spice-gtk"|"usbredir")
            ~/src/ci/build-in-copr.sh `ls *tar* | head -n1` @spice/nightly "mingw-$DIR"
            ;;
    esac
    make install
  else
    echo "$DIR is probably already up-to-date" >> /var/log/copr.log
  fi
  git clean -xfd
  cd ..
done

cd mingw-spice-vdagent
git fetch && git pull | grep -q -v 'Already up-to-date.'  &&  git clean -xfd  && NOCONFIGURE=1 ./autogen.sh && mingw64-configure --disable-werror && make dist && ~/src/ci/build-in-copr.sh `ls *tar* | head -n1` @spice/nightly mingw-spice-vdagent
if [[ "$?" = "0" ]]; then
  echo "mingw-spice-vdagent was built" >> /var/log/copr.log
else
  echo "mingw-spice-vdagent is probably already up-to-date" >> /var/log/copr.log
fi
