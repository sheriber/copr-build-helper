Name: $PKG_NAME
Version: $PKG_VERSION
Release: $TIMESTAMP$PKG_RELEASE%{?dist}

Summary: A WebDAV server using libsoup

Group: Applications/Internet
License: LGPLv2+
URL: https://wiki.gnome.org/phodav

Source0: $PKG_ARCHIVE

BuildRequires:  systemd-devel
BuildRequires:  systemd-units
BuildRequires:  libsoup-devel
BuildRequires:  avahi-gobject-devel
BuildRequires:  intltool
BuildRequires:  asciidoc
BuildRequires:  xmlto
# automake-1.13 is triggered during make, F21 has 1.14
BuildRequires:  autoconf automake libtool

%description
phởdav is a WebDAV server implementation using libsoup (RFC 4918).

%package -n     libphodav
Summary:        A library to serve files with WebDAV
Provides:       libphodav-2.0 = 0:%{version}-%{release}
Provides:       libphodav2 = 0:%{version}-%{release}
Obsoletes:      libphodav-2.0 <= 0:2.0-3
Obsoletes:      libphodav2 <= 0:2.0-4
# no Provides for this one as ABI was broken
Obsoletes:      libphodav-1.0 <= 0:0.4-6

%description -n libphodav
phởdav is a WebDAV server implementation using libsoup (RFC 4918).
This package provides the library.

%package -n     libphodav-devel
Summary:        Development files for libphodav
Requires:       libphodav%{?_isa} = %{version}-%{release}
Provides:       libphodav-2.0-devel = 0:%{version}-%{release}
Provides:       libphodav2-devel = 0:%{version}-%{release}
Obsoletes:      libphodav-2.0-devel <= 0:2.0-3
Obsoletes:      libphodav2-devel <= 0:2.0-4
# no Provides for this one as ABI was broken
Obsoletes:      libphodav-1.0-devel <= 0:0.4-6

%description -n libphodav-devel
The libphodav-devel package includes the header files for libphodav.

%package -n     chezdav
Summary:        A simple WebDAV server program
Group:          Applications/Internet

%description -n chezdav
The chezdav package contains a simple tool to share a directory
with WebDAV. The service is announced over mDNS for clients to discover.

%package -n     spice-webdavd
Summary:        Spice daemon for the DAV channel
Group:          Applications/System
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description -n spice-webdavd
The spice-webdavd package contains a daemon to proxy WebDAV request to
the Spice virtio channel.

%prep
%setup -q -n $PKG_ARCHIVE_NO_EXTENSION

%build
%configure --with-avahi
make %{?_smp_mflags}

%install
%make_install

rm -f %{buildroot}%{_libdir}/*.a
rm -f %{buildroot}%{_libdir}/*.la

%find_lang phodav-2.0 --with-gnome

%post -n libphodav -p /sbin/ldconfig
%postun -n libphodav -p /sbin/ldconfig

%post -n spice-webdavd
%systemd_post spice-webdavd.service

%preun -n spice-webdavd
%systemd_preun spice-webdavd.service

%postun -n spice-webdavd
%systemd_postun_with_restart spice-webdavd.service

%files -n libphodav -f phodav-2.0.lang
%doc NEWS COPYING
%{_libdir}/libphodav-2.0.so.0*

%files -n libphodav-devel
%dir %{_includedir}/libphodav-2.0/
%{_includedir}/libphodav-2.0/*
%{_libdir}/libphodav-2.0.so
%{_libdir}/pkgconfig/libphodav-2.0.pc
%{_datadir}/gtk-doc/html/phodav-2.0/*

%files -n chezdav
%{_bindir}/chezdav
%{_mandir}/man1/chezdav.1*

%files -n spice-webdavd
%doc NEWS COPYING
%{_sbindir}/spice-webdavd
/usr/lib/udev/rules.d/70-spice-webdavd.rules
%{_unitdir}/spice-webdavd.service
