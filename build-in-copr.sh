#!/usr/bin/env bash

set -e

PKG_ARCHIVE=${1-`ls *tar* | head -n1`}
COPR_REPO=${2-"@spice/nightly"}
COPR_REPO_SUFFIX=`echo "$COPR_REPO" | tr "@/-" "."`
DIST="epel7"

SCRIPT_DIR=`which $0`
SCRIPT_DIR=`dirname $SCRIPT_DIR`

cd `dirname $PKG_ARCHIVE`
PKG_ARCHIVE=`basename $PKG_ARCHIVE`
PKG_NAME=`echo "$PKG_ARCHIVE" | grep -o "^[a-z0-9\-]*[a-z]\+" `
PKG_VERSION=`echo "$PKG_ARCHIVE" | grep -o "${PKG_NAME}-[a-f0-9\.]\+[a-f0-9]\+" | grep -o "[a-f0-9\.]*$"`
PKG_ARCHIVE_NO_EXTENSION=`echo "$PKG_ARCHIVE" | sed s,.tar.*$,, | sed s,.zip.*$,,`
PKG_RELEASE=`echo "$PKG_ARCHIVE_NO_EXTENSION" | grep -o "[a-f0-9]*$" || echo "1"`
PKG_RELEASE+=".$COPR_REPO_SUFFIX"
PKG_RELEASE=`echo ".$PKG_RELEASE" | tr -s "." `

TIMESTAMP=`date +"%Y%m%d%H%M"`

PKG_NAME_FORCE=${3-$PKG_NAME}
PKG_NAME=$PKG_NAME_FORCE

rm -rf *.spec
eval "cat > $PKG_NAME.spec <<END_OF_SPEC_TMPL
$(< $SCRIPT_DIR/$PKG_NAME.tmpl)

%changelog
* `date +\"%a %b %e %Y\"` SPICE Development Team <spice-devel@lists.freedesktop.org> - $PKG_VERSION-$TIMESTAMP$PKG_RELEASE
- Build from git commmit `git rev-parse --verify --quiet HEAD` in $COPR_REPO copr
END_OF_SPEC_TMPL"

md5sum "$PKG_ARCHIVE" > "sources"

fedpkg --release "$DIST" --path `dirname $PKG_ARCHIVE` srpm
fedpkg --release "$DIST" prep && copr build --nowait --background "$COPR_REPO" "$PKG_NAME-$PKG_VERSION-$TIMESTAMP$PKG_RELEASE.el7.src.rpm"

rm "sources" "$PKG_NAME-$PKG_VERSION-$TIMESTAMP$PKG_RELEASE.el7.src.rpm" "$PKG_NAME.spec"
